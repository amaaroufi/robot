#!/bin/bash
# from https://chromium.woolyss.com/
# and https://gist.github.com/addyosmani/5336747
# and https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md

if [ -z $(which node) ]; then
    curl -sL https://deb.nodesource.com/setup_8.x | bash -
    apt-get install -y nodejs
fi

if [ -z $(which npm) ]; then
    apt-get install -y npm
fi

apt-get install -y libappindicator1 fonts-liberation
apt-get -y install dbus-x11 xfonts-base xfonts-100dpi xfonts-75dpi xfonts-cyrillic xfonts-scalable

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome*.deb
apt-get -f -y install
dpkg --configure -a
dpkg -i google-chrome*.deb

npm install chromedriver -g --unsafe-perm=true --allow-root
pip install --user robotframework-selenium2library