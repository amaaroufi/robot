*** variables ***
${createConference}  /Conference/CreateConference.aspx
${myConferences}  /Conference/MyConferences.aspx
${conferenceName}  //*[@id="CC_tn"]
${conferencePwd}  //*[@id="CC_tp_txt"]
${conferencePwdConfirm}  //*[@id="CC_tcp_txt"]
${conferenceStartDate}  //*[@id="CC_datProg_txt"]
${conferenceCreateBtn}  //*[@id="lv"]
${deleteConfrerence}  //*[@id="btbConferenceList"]//span[contains(text(),'supprimer')]