*** Settings ***
Library  SeleniumLibrary  30
Resource  Configuration_globale.robot
Resource  locators.robot

*** Keywords ***
Se connecter a Gassi
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}

    go to   https://gitlab.com/projects/new

    Wait Until Element Is Visible    css:#user_login
    Input Text    css:#user_login    dolimoni
    Wait Until Element Is Visible    css:#user_password
    Input Text    css:#user_password    kei110692

    Click Element  css:#new_user > div.submit-container.move-submit-down > input


    
    Wait Until Element Is Visible    css:#project_name
    Input Text    css:#project_name    test khalid 5
    Click Element    css:#new_project > input.btn.btn-success.project-submit
    

    Log to console  "Connected to Gassi"
    
Choisir une application
    [Arguments]  ${Application}
    
    Log to console  ${Application}


    sleep  5s
    Select Window    Mon SI : l'accueil dans le Système d'Information     
    sleep  5s

    Log to console  Mon SI : l'accueil dans le Système d'Information
    
    
    #Element Should Be Visible    tab_si
    Wait Until Element Is Visible    //a[contains(text(),'${Application}')]
    Click Element    //a[contains(text(),'${Application}')]
    Select Window    New
    
    Unselect Frame
    
Select menu item
    [Arguments]  ${item}
    Wait Until Element Is Visible    xpath=//a[@href="${item}"]    
    Click Element   xpath=//a[@href="${item}"]
    
Select occupation from menu
    
    Wait Until Element Is Visible    //a[contains(text(),'Occupation')]    
    Click Element   //a[contains(text(),'Occupation')] 
    
    Wait Until Element Is Visible    //a[contains(text(),'Demande de création PSI')]    
    Click Element   //a[contains(text(),'Demande de création PSI')]
    
Choisir PDR
    [Arguments]  ${item}
    Wait Until Element Is Visible  css:#prj_recherche
    Input Text    css:#prj_recherche    ${item}
   
    Wait Until Element Is Visible  //*[@id="demProjetForm"]/div[2]/fieldset/div[1]/div[2]/ul
    
    Execute JavaScript  $("#demProjetForm > div:nth-child(2) > fieldset > div:nth-child(2) > div.col-xs-12.col-sm-4.text-left.text-align-responsive > ul > li > a").trigger("click");

Donnees de facturation
    Wait Until Element Is Visible  css:#cdr_recherche
    Input Text    css:#cdr_recherche    NL100
    
    Wait Until Element Is Visible  css:#demProjetForm > div:nth-child(4) > div:nth-child(2) > ul > li
    Execute JavaScript  $("#demProjetForm > div:nth-child(4) > div:nth-child(2) > ul > li").trigger("click");

Occupation [P] PSI
    Wait Until Element Is Visible  css:#dmp_prj_nom
    Input Text    css:#dmp_prj_nom    test_robot_framework

    Wait Until Element Is Visible  css:#rsol
    Input Text    css:#rsol    essalhi khalid
    
    Wait Until Element Is Visible  css:#demProjetForm > div:nth-child(10) > div:nth-child(4) > ul > li
    Execute JavaScript  $("#demProjetForm > div:nth-child(10) > div:nth-child(4) > ul > li").trigger("click");
    
    Select From List By Value  css:#ds_id  4573
    
    Wait Until Element Is Visible    css:#dmp_occ_deb_date
    Input Text  css:#dmp_occ_deb_date  01/09/2019
    
    Wait Until Element Is Visible    css:#dmp_occ_descr    
    Input Text  css:#dmp_occ_descr  Description occupation transférée automatiquement dans Qualis : Test robot
    
    
    Wait Until Element Is Visible    css:#dmp_commentaire    
    Input Text  css:#dmp_commentaire  Commentaires d’échange entre le demandeur , le RS et l'administrateur : Test robot
    
    Click Element   css:#soumettre_rs   
    
    
    
    
    
    
    